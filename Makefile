.DEFAULT_GOAL:=help

dev: ## Deploy dev environment.
	docker-compose -f docker-compose.yml up -d

stop: ## Stop the environment running.
	docker-compose stop

help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m (default: help)\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-12s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)